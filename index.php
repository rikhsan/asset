<?php
try{
    if(!isset($_SERVER['HTTP_KEY']) || $_SERVER['HTTP_KEY'] !== 'secretkey')
        throw new ErrorException('Unauthorized');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $app =  $_POST['app'];
        $dir =  $_POST['dir'];
        $file =  $_FILES['file'];

        $directory = sprintf("%s/%s", $app, $dir);

        if (!file_exists($directory))
            mkdir($directory, 0777, true);

        $path = sprintf("%s/%s", $directory, $file['name']);

        if (!move_uploaded_file($file['tmp_name'], $path))
            throw new ErrorException();

        echo $path;
    }else if ($_SERVER['REQUEST_METHOD'] === 'DELETE'){
        $body = file_get_contents('php://input');
        if (file_exists($body))
            unlink($body);
        http_response_code(204);
    }else if ($_SERVER['REQUEST_METHOD'] === 'GET') 
        echo 'SERVER ASSET';
    else 
        throw new ErrorException();
}catch(Exception $e){
    // echo $e->getMessage();
    http_response_code(500);
}
